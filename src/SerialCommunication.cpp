//
// Created by cedu on 3/18/20.
//

#include "SerialCommunication.h"

char SerialCommunication::startMarker = '<';
char SerialCommunication::endMarker  = '>';

TaskEnum SerialCommunication::getTask() {
    String message = readSerialPort();
    if(message.equals("")){
        return TaskEnum::NoOpenTask;
    }
    auto result = static_cast<TaskEnum>(message.toInt());
    return result;
}

String SerialCommunication::readSerialPort() {
    const byte numChars = 32;
    char receivedChars[numChars];
    boolean newData = false;
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char rc;

    while (Serial.available() > 0 && !newData) {
        rc = Serial.read();
        if (recvInProgress != 0) {
            if (rc != endMarker) {
                receivedChars[ndx] = rc;
                ndx++;
                if (ndx >= numChars) {
                    ndx = numChars - 1;
                }
            }
            else {
                receivedChars[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                ndx = 0;
                newData = true;
            }
        }

        else if (rc == startMarker) {
            recvInProgress = true;
        }
    }
    if(newData){
        return receivedChars;
    }
    return "";
}

void SerialCommunication::sendResult(String result) {
    Serial.println(startMarker + result + endMarker);
}