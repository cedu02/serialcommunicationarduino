//
// Created by cedu on 3/22/20.
//

#ifndef STRAWBERRZ_TASKENUM_H
#define STRAWBERRZ_TASKENUM_H

enum class TaskEnum{
    StartWaterPump = 1,
    StopWaterPump = 2,
    StartFan = 3,
    StopFan = 4,
    StartLight = 5,
    StopLight = 6,
    ReadTemperatureHumidity = 7,
    ReadWaterLevelReservoir = 8,
    ReadWaterLevelTray = 9,
    ReadHumidityPlants = 10,
    StartAirPump = 11,
    StopAirPump = 12,
    ReadUVLevel = 13,
    ReadWaterTemp = 14,
    ReadPHValue = 15,
    ReadTDSValue = 16,
    ReadTemperatureHumidityMiddle = 17,
    ReadAirQuality = 18,
    ReadPressure = 19,
    ReadLux = 20,
    NoOpenTask = 999
};
#endif //STRAWBERRZ_TASKENUM_H
